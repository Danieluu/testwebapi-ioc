using System;
using System.Linq.Expressions;
using MongoDB.Driver;

namespace TestWebApi.Extesions
{
    public static class MongoExtensions
    {
        public static void InsertOne<T>(this IMongoCollection<T> collection, Expression<Func<T, bool>> expression, T value) where T : class
        {
            collection.InsertOne(value);
        }
    }
}