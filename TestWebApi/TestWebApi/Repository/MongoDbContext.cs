using Microsoft.Extensions.Options;
using MongoDB.Driver;
using TestWebApi.Cofigs;
using TestWebApi.Domain;

namespace TestWebApi.Repository
{
    public class MongoDbContext
    {
        public readonly IMongoDatabase Database;

        public MongoDbContext(Settings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            Database = client.GetDatabase(settings.Database);
        }
    }
}