using System.Collections.Generic;
using System.Threading.Tasks;
using TestWebApi.Domain;

namespace TestWebApi.Repository
{
    public interface IMongoRepository
    {
        IEnumerable<TType> GetAll<TType>() where TType : class,new();
        TType Get<TType>(string name) where TType : class;
        void Add<TType>(TType game) where TType : class,new();
    }
}