using System.Collections.Generic;
using MongoDB.Driver;
using TestWebApi.Extesions;
namespace TestWebApi.Repository
{
    public class MongoRepository : IMongoRepository
    {
        private readonly MongoDbContext _context;
        public MongoRepository(MongoDbContext context)
        {
            _context = context;
        }

        public void Add<TType>(TType value) where TType : class, new()
        {
            _context.Database.GetCollection<TType>(typeof(TType).Name).InsertOne<TType>(e => true, value);
        }

        public TType Get<TType>(string name) where TType : class
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<TType> GetAll<TType>() where TType : class, new()
        {
            return _context.Database.GetCollection<TType>(typeof(TType).Name).Find(_ => true).ToList();
        }
    }
}