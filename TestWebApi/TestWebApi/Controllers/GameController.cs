using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TestWebApi.Domain;
using TestWebApi.Repository;

namespace TestWebApi.Controllers
{
    [Route("api/[controller]/[action]")]
    public class GameController : ControllerBase
    {
        [HttpGet]
        public ActionResult<string> Test()
        {
            return "Test";
        }

        // GET api/game
        [HttpGet]
        public IEnumerable<Game> Gets([FromServices] IMongoRepository repo)
        {
            return repo.GetAll<Game>();
        }

        // // GET api/game/5
        // [HttpGet("{id}")]
        // public ActionResult<string> GetById(int id)
        // {
        //     return "value" + id;
        // }

        // POST api/game
        [HttpPost]
        public void Post([FromBody] Game game, [FromServices] IMongoRepository repo)
        {
            repo.Add(game);
        }

        // PUT api/game/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value) { }

        // DELETE api/game/5
        [HttpDelete("{id}")]
        public void DeleteById(int id) { }
    }
}