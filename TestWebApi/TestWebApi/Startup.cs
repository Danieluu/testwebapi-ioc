﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using TestWebApi.Cofigs;
using TestWebApi.Repository;

namespace TestWebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();

            // One way of bind appsettings
            // In MongoDbContext use IOptions get value
            // var section = Configuration.GetSection("MongoDb");
            // services.Configure<Settings>(section);

            // One way of bind appsettings
            // In MongoDbContext direct get settings, this way is better than using IOptions
            var settings = new Settings();
            Configuration.Bind("MongoDb",settings);
            services.AddSingleton(settings);

            // One way of bind appsettings
            // In MongoDbContext use IOptions get value
            // services.Configure<Settings>(options => {
            //     options.ConnectionString = Configuration.GetSection("MongoDb:ConnectionString").Value;
            //     options.Database = Configuration.GetSection("MongoDb:Database").Value;
            // });
            
            services.AddSingleton<MongoDbContext>();
            services.AddTransient<IMongoRepository, MongoRepository>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
