using System;
using MongoDB.Bson.Serialization.Attributes;

namespace TestWebApi.Domain
{
    public class Game
    {
        [BsonId]
        public string Id { get; set; }
        public string Name { get; set; }
    }
}